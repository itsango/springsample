package jp.ne.zaq.rinku.bkbin005.springSample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author <a href="mailto:bkbin005@rinku.zaq.ne.jp">Mitsutoshi NAKANO</a>
 * @see <a href="http://catacataog.com/hello-world-spring-legacy-project/">
 * http://catacataog.com/hello-world-spring-legacy-project/</a>
 *
 */
@Controller
public class SampleController {
    @RequestMapping(value = "showMessage.html" , method = RequestMethod.GET)
    public String hello(Model model) {
        model.addAttribute("message", "Hello World!");
        return "showMessage";
    }
}
